# Remedy Action Request

Vendor: BMC
Homepage: https://www.bmc.com/

Product: Remedy Action Request
Product Page: https://docs.bmc.com/docs/ars2002

## Introduction
We classify Remedy Action Request into the ITSM (Service Management) domain as Remedy Action Request provides core ITSM functions like maitaining compliance, governance, service request management, etc. 

## Why Integrate
The Remedy Action Request adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Remedy AR System to automate and streamline key IT service processes. With this adapter you have the ability to perform operations such as:

- Get, Update, Create, or Delete Entry
- Get, Update, or Register Webhook

## Additional Product Documentation
The [API documents for Remedy Action Request](https://docs.bmc.com/docs/itsm2002/overview-of-the-rest-api-932206744.html)