
## 0.5.0 [05-25-2023]

* ADAPT-1868 updated with API calls

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!7

---

## 0.4.1 [09-09-2022]

* Fix the versioning which got messed up with replacing the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!6

---

## 0.2.1 [09-09-2022]

* Update version due to publish issue

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!5

---

## 0.3.0 [05-29-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!3

---

## 0.2.0 [12-29-2021]

- Add login and logout locally in adapter.js

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!1

---

## 0.1.2 [10-24-2021] & 0.1.1 [10-24-2021]

- Initial Commit

See commit ca792eb

---
