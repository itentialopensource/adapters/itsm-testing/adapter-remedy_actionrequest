
## 0.7.4 [10-15-2024]

* Changes made at 2024.10.14_20:18PM

See merge request itentialopensource/adapters/adapter-remedy_actionrequest!21

---

## 0.7.3 [09-15-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-remedy_actionrequest!19

---

## 0.7.2 [08-14-2024]

* Changes made at 2024.08.14_18:28PM

See merge request itentialopensource/adapters/adapter-remedy_actionrequest!18

---

## 0.7.1 [08-07-2024]

* Changes made at 2024.08.06_19:41PM

See merge request itentialopensource/adapters/adapter-remedy_actionrequest!17

---

## 0.7.0 [07-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!16

---

## 0.6.6 [05-01-2024]

* Patch/metachanges

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!15

---

## 0.6.5 [03-27-2024]

* Changes made at 2024.03.27_14:01PM

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!13

---

## 0.6.4 [03-13-2024]

* Changes made at 2024.03.13_15:03PM

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!12

---

## 0.6.3 [03-11-2024]

* Changes made at 2024.03.11_14:44PM

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!11

---

## 0.6.2 [02-28-2024]

* Changes made at 2024.02.28_11:08AM

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!10

---

## 0.6.1 [12-31-2023]

* update links

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!9

---

## 0.6.0 [12-31-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!8

---

## 0.5.0 [05-25-2023]

* ADAPT-1868 updated with API calls

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!7

---

## 0.4.1 [09-09-2022]

* Fix the versioning which got messed up with replacing the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!6

---

## 0.2.1 [09-09-2022]

* Update version due to publish issue

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!5

---

## 0.3.0 [05-29-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!3

---

## 0.2.0 [12-29-2021]

- Add login and logout locally in adapter.js

See merge request itentialopensource/adapters/itsm-testing/adapter-remedy_actionrequest!1

---

## 0.1.2 [10-24-2021] & 0.1.1 [10-24-2021]

- Initial Commit

See commit ca792eb

---
